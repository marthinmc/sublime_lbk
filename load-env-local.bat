@echo off

Set src="\\RICDESALBK.lbkitmad.local\DesarrolloSPA\LBK_ARQ_SPA"
Set srcconf="\\RICDESALBK.lbkitmad.local\DesarrolloSPA\conf_env_local"
Set dest="D:\LBK_ARQ_SPA"
Set destconf="C:\LBKIT\lbk-front-config\sublime"

set folderlog=%userprofile%\Documents\sublime-logs\
set ficherolog=sublime_lbk.log

if exist %folderlog% (
    del /F /Q %folderlog%%ficherolog%
) else (
    mkdir %folderlog%
)

if exist %dest%\public\ltf\ (
	echo "Deleting ltf"
	rd /s /q %dest%\public\ltf
)
if exist %dest%\public\lar\ (
	echo "Deleting lar"
	rd /s /q %dest%\public\lar
)
if exist %dest%\public\la1\ (
	echo "Deleting la1"
	rd /s /q %dest%\public\la1
)

set _options=/MT:16 /ns /r:0 /w:0 /LOG+:%folderlog%%ficherolog%

copy %srcconf%\batch\create_nm.bat %destconf%\batch\create_nm.bat
copy %srcconf%\batch\create_nm_2.bat %destconf%\batch\create_nm_2.bat

Robocopy %srcconf%\linters-config %destconf%\linters-config /MIR %_options%
if %ERRORLEVEL% lss 8 goto next
	echo "Error. Robocopy exit code %ERRORLEVEL%" & pause & goto :eof
:next
	Robocopy %srcconf%\templates %destconf%\templates /MIR %_options%
	if %ERRORLEVEL% lss 8 goto next
		echo "Error. Robocopy exit code %ERRORLEVEL%" & pause & goto :eof
	:next
		Robocopy %src% %dest% /MIR /XD %src%\public %_options%
		if %ERRORLEVEL% lss 8 goto next
			echo "Error. Robocopy exit code %ERRORLEVEL%" & pause & goto :eof
		:next
			Robocopy %src%\public\lqv %dest%\public\lqv /MIR /XD %src%\public\lqv\config\data %_options%
			if %ERRORLEVEL% lss 8 goto next
				echo "Error. Robocopy exit code %ERRORLEVEL%" & pause & goto :eof
			:next
				if not exist %dest%\public\lqv\config\data\menus.json (
					echo "Copy file menus.json"
					mkdir %dest%\public\lqv\config\data
					copy %src%\public\lqv\config\data\menus.json %dest%\public\lqv\config\data\menus.json
				)
				Robocopy %src%\public\lqp %dest%\public\lqp /MIR %_options%
				if %ERRORLEVEL% lss 8 goto next
					echo "Error. Robocopy exit code %ERRORLEVEL%" & pause & goto :eof
				:next
					Robocopy %src%\public\lqw %dest%\public\lqw /MIR %_options%
					if %ERRORLEVEL% lss 8 goto next
						echo "Error. Robocopy exit code %ERRORLEVEL%" & pause & goto :eof
					:next
						Robocopy %srcconf%\install "%userprofile%\AppData\Roaming\Sublime Text 3" /MIR /XD %srcconf%\install\Local %_options%
						if %ERRORLEVEL% lss 8 goto finish
							echo "Error. Robocopy exit code %ERRORLEVEL%" & pause & goto :eof

						:finish
							echo "Robocopy exit code %ERRORLEVEL%"
							echo Abriendo Sublime ...
							start /D "%ProgramFiles%\Sublime Text 3" sublime_text.exe
							rem pause
							exit
